package com.twuc.webApp;

import com.twuc.webApp.entity.Reservation;
import com.twuc.webApp.entity.Staff;
import com.twuc.webApp.repository.ReservationRepository;
import com.twuc.webApp.repository.StaffRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.Duration;
import java.time.ZoneId;
import java.time.ZonedDateTime;

import static org.junit.jupiter.api.Assertions.*;

public class ReservationEntityRepositoryTest extends JpaTestBase{
    @Autowired
    private ReservationRepository reservationRepository;

    @Autowired
    private StaffRepository staffRepository;

    @Test
    void should_save_the_relationship_from_the_staff_side() {

        Staff staff = new Staff("James", "Bond", "Africa/Nairobi");
        Reservation reservation = new Reservation("Sofia",
                "ThoughtWorks",
                "Africa/Nairobi",
                ZonedDateTime.of(2019, 8, 20, 16, 46, 0, 0, ZoneId.of("Africa/Nairobi")),
                Duration.ofHours(1));

        staff.getReservations().add(reservation);
        reservation.setStaff(staff);

        flushAndClear(em -> {
            staffRepository.save(staff);
        });

        Reservation fetchedReservation = reservationRepository.findById(reservation.getId()).get();
        Staff fetchedStaff = staffRepository.findById(staff.getId()).get();
        assertEquals(1,fetchedStaff.getReservations().size());
        assertEquals(Long.valueOf(1),fetchedReservation.getId());;
    }


}
