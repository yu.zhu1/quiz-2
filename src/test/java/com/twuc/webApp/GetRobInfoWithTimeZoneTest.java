package com.twuc.webApp;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.entity.Staff;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;


import static org.junit.jupiter.api.Assertions.assertNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class GetRobInfoWithTimeZoneTest extends ApiTestBase {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void should_return_200_when_put_time_zone_for_staff_if_success() throws Exception {

        mockMvc.perform(post("/api/staffs")
                .contentType("application/json")
                .content("{ \"firstName\": \"Rob\",\"lastName\": \"Hall\"}"));

        mockMvc.perform(put("/api/staffs/1/timezone").contentType("application/json").content("{\"zoneId\":\"Asia/Chongqing\"}"))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$.zoneId").value("Asia/Chongqing"));
    }

    @Test
    void should_return_400_if_zone_id_is_null() throws Exception {
        mockMvc.perform(post("/api/staffs")
                .contentType("application/json")
                .content("{ \"firstName\": \"Rob\",\"lastName\": \"Hall\"}"));

        mockMvc.perform(put("/api/staffs/1/timezone"))
                .andExpect(status().is(400));
    }

    @Test
    void should_return_400_if_zone_id_is_of_wrong_type() throws Exception {
        mockMvc.perform(post("/api/staffs")
                .contentType("application/json")
                .content("{ \"firstName\": \"Rob\",\"lastName\": \"Hall\"}"));

        mockMvc.perform(put("/api/staffs/1/timezone").contentType("application/json").content("{\"zoneId\":\"asi/chongqing\"}"))
                .andExpect(status().is(400));
    }



    @Test
    void should_return_content_with_time_zone_id_when_GET_api() throws Exception {
        mockMvc.perform(post("/api/staffs")
                .contentType("application/json")
                .content("{ \"firstName\": \"Rob\",\"lastName\": \"Hall\",\"zoneId\":\"Asia/Chongqing\"}"));


        mockMvc.perform(get("/api/staffs/1"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.firstName").value("Rob"))
                .andExpect(jsonPath("$.lastName").value("Hall"))
                .andExpect(jsonPath("$.zoneId").value("Asia/Chongqing"));

    }

    @Test
    void should_return_content_with_time_zone_of_null_when_GET_api_if_not_specified() throws Exception {
        mockMvc.perform(post("/api/staffs")
                .contentType("application/json")
                .content("{ \"firstName\": \"Rob\",\"lastName\": \"Hall\"}"));


        MockHttpServletResponse response = mockMvc.perform(get("/api/staffs/1"))
                .andExpect(status().is(200)).andReturn().getResponse();

        ObjectMapper objectMapper = new ObjectMapper();
        Staff staff = objectMapper.readValue(response.getContentAsString(), Staff.class);
        assertNull(staff.getZoneId());
    }
}
