package com.twuc.webApp;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.twuc.webApp.contract.ReservationList;
import com.twuc.webApp.entity.Staff;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.bind.annotation.ExceptionHandler;


import java.time.Duration;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

public class ReservationTest extends ApiTestBase {

    @Autowired
    private MockMvc mockMvc;


    @Test
    void should_return_status_201_if_add_reservations_success() throws Exception {
        mockMvc.perform(post("/api/staffs")
                .contentType("application/json")
                .content("{ \"firstName\": \"Rob\",\"lastName\": \"Hall\"}"));

        mockMvc.perform(put("/api/staffs/1/timezone").contentType("application/json").content("{\"zoneId\":\"Asia/Chongqing\"}"));

        mockMvc.perform(post("/api/staffs/1/reservations").contentType("application/json").content("{\n" +
                "  \"userName\": \"Sofia\",\n" +
                "  \"companyName\": \"ThoughtWorks\",\n" +
                "  \"zoneId\": \"Africa/Nairobi\",\n" +
                "  \"startTime\": \"2019-10-20T11:46:00+03:00\",\n" +
                "  \"duration\": \"PT1H\"\n" +
                "}"))
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.header().string("Location","/api/staffs/1/reservations"));
    }

    @Test
    void should_get_reservation_list_from_specific_staff_when_GET_api() throws Exception {
        mockMvc.perform(post("/api/staffs")
                .contentType("application/json")
                .content("{ \"firstName\": \"Rob\",\"lastName\": \"Hall\"}"));

        mockMvc.perform(put("/api/staffs/1/timezone").contentType("application/json").content("{\"zoneId\":\"Asia/Chongqing\"}"));

        mockMvc.perform(post("/api/staffs/1/reservations").contentType("application/json").content("{\n" +
                "  \"userName\": \"Sofia\",\n" +
                "  \"companyName\": \"ThoughtWorks\",\n" +
                "  \"zoneId\": \"Africa/Nairobi\",\n" +
                "  \"startTime\": \"2019-10-20T11:46:00+03:00\",\n" +
                "  \"duration\": \"PT1H\"\n" +
                "}"));

        MockHttpServletResponse response = mockMvc.perform(get("/api/staffs/1/reservations"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andReturn().getResponse();
        List<ReservationList> reservationList = getReservationList(response);
        assertEquals("Sofia",reservationList.get(0).getUserName());
        assertEquals("ThoughtWorks", reservationList.get(0).getCompanyName());
        assertEquals(Duration.ofHours(1),reservationList.get(0).getDuration());
        assertNotNull(reservationList.get(0).getStartTime().getClient());
        assertNotNull(reservationList.get(0).getStartTime().getStaff());
//        assert Equals("2019-10-20T08:46Z[UTC]",reservationList.get(0).getStartTime().getStaff().getStartTime());
        System.out.println(response.getContentAsString());

    }

    private List<ReservationList> getReservationList(MockHttpServletResponse response) throws java.io.IOException {
        ObjectMapper objectMapper = new ObjectMapper();

        objectMapper.registerModule(new JavaTimeModule());
        objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        return objectMapper.reader()
                .forType(new TypeReference<List<ReservationList>>() {
                })
                .readValue(response.getContentAsString());
    }


    @Test
    void should_return_409_if_time_zone_id_not_provided_when_add_reservation() throws Exception {
        mockMvc.perform(post("/api/staffs")
                .contentType("application/json")
                .content("{ \"firstName\": \"Rob\",\"lastName\": \"Hall\"}"));

        mockMvc.perform(post("/api/staffs/1/reservations").contentType("application/json").content("{\n" +
                "  \"userName\": \"Sofia\",\n" +
                "  \"companyName\": \"ThoughtWorks\",\n" +
                "  \"zoneId\": \"Africa/Nairobi\",\n" +
                "  \"startTime\": \"2019-10-20T11:46:00+03:00\",\n" +
                "  \"duration\": \"PT1H\"\n" +
                "}"))
                .andExpect(MockMvcResultMatchers.status().is(409))
                .andExpect(jsonPath("$.message").value("Rob Hall is not qualified."));

    }

    @Test
    void should_return_400_if_add_reservation_with_48h_from_now() throws Exception {
        mockMvc.perform(post("/api/staffs")
                .contentType("application/json")
                .content("{ \"firstName\": \"Rob\",\"lastName\": \"Hall\"}"));

        mockMvc.perform(put("/api/staffs/1/timezone").contentType("application/json").content("{\"zoneId\":\"Asia/Chongqing\"}"));

        mockMvc.perform(post("/api/staffs/1/reservations").contentType("application/json").content("{\n" +
                "  \"userName\": \"Sofia\",\n" +
                "  \"companyName\": \"ThoughtWorks\",\n" +
                "  \"zoneId\": \"Africa/Nairobi\",\n" +
                "  \"startTime\": \"2019-10-12T11:46:00+03:00\",\n" +
                "  \"duration\": \"PT1H\"\n" +
                "}"))
                .andExpect(MockMvcResultMatchers.status().is(400))
                .andExpect(jsonPath("$.message").value("Invalid time: the application is too close from now. The interval should be greater than 48 hours."));
    }


    @Test
    void should_return_409_if_add_reservation_outside_working_schedule_of_the_staff() throws Exception {
        mockMvc.perform(post("/api/staffs")
                .contentType("application/json")
                .content("{ \"firstName\": \"Rob\",\"lastName\": \"Hall\"}"));

        mockMvc.perform(put("/api/staffs/1/timezone").contentType("application/json").content("{\"zoneId\":\"Asia/Chongqing\"}"));

        mockMvc.perform(post("/api/staffs/1/reservations").contentType("application/json").content("{\n" +
                "  \"userName\": \"Sofia\",\n" +
                "  \"companyName\": \"ThoughtWorks\",\n" +
                "  \"zoneId\": \"Africa/Nairobi\",\n" +
                "  \"startTime\": \"2019-10-30T23:46:00+03:00\",\n" +
                "  \"duration\": \"PT1H\"\n" +
                "}"))
                .andExpect(MockMvcResultMatchers.status().is(409))
                .andExpect(jsonPath("$.message").value("You know, our staff has their own life."));
    }

    @ParameterizedTest
    @ValueSource(doubles = {1.5, 10})
    void should_return_400_if_duration_is_not_integer(double duration) throws Exception {
        mockMvc.perform(post("/api/staffs")
                .contentType("application/json")
                .content("{ \"firstName\": \"Rob\",\"lastName\": \"Hall\"}"));

        mockMvc.perform(put("/api/staffs/1/timezone").contentType("application/json").content("{\"zoneId\":\"Asia/Chongqing\"}"));

        mockMvc.perform(post("/api/staffs/1/reservations").contentType("application/json").content(String.format(
                "{\n" +
                        "  \"userName\": \"Sofia\",\n" +
                        "  \"companyName\": \"ThoughtWorks\",\n" +
                        "  \"zoneId\": \"Africa/Nairobi\",\n" +
                        "  \"startTime\": \"2019-10-20T11:46:00+03:00\",\n" +
                        "  \"duration\": \"PT%fH\"\n" +
                        "}",duration
        )))
                .andExpect(MockMvcResultMatchers.status().is(400))
                .andExpect(jsonPath("$.message").value( "The application duration should be within 1-3 hours."));

    }

    @ParameterizedTest
    @ValueSource(ints = {0, 10})
    void should_return_400_if_duration_is_outside_1_and_3(int duration) throws Exception {
        mockMvc.perform(post("/api/staffs")
                .contentType("application/json")
                .content("{ \"firstName\": \"Rob\",\"lastName\": \"Hall\"}"));

        mockMvc.perform(put("/api/staffs/1/timezone").contentType("application/json").content("{\"zoneId\":\"Asia/Chongqing\"}"));

        mockMvc.perform(post("/api/staffs/1/reservations").contentType("application/json").content(String.format(
                "{\n" +
                        "  \"userName\": \"Sofia\",\n" +
                        "  \"companyName\": \"ThoughtWorks\",\n" +
                        "  \"zoneId\": \"Africa/Nairobi\",\n" +
                        "  \"startTime\": \"2019-10-20T11:46:00+03:00\",\n" +
                        "  \"duration\": \"PT%dH\"\n" +
                        "}",duration
        )))
                .andExpect(MockMvcResultMatchers.status().is(400))
                .andExpect(jsonPath("$.message").value( "The application duration should be within 1-3 hours."));
    }


    @Test
    void should_return_409_if_add_reservation_and_get_conflicts_of_reserved_schedules_for_staff() throws Exception {

        mockMvc.perform(post("/api/staffs")
                .contentType("application/json")
                .content("{ \"firstName\": \"Rob\",\"lastName\": \"Hall\"}"));

        mockMvc.perform(put("/api/staffs/1/timezone").contentType("application/json").content("{\"zoneId\":\"Asia/Chongqing\"}"));

        mockMvc.perform(post("/api/staffs/1/reservations").contentType("application/json").content("{\n" +
                "  \"userName\": \"Sofia\",\n" +
                "  \"companyName\": \"ThoughtWorks\",\n" +
                "  \"zoneId\": \"Africa/Nairobi\",\n" +
                "  \"startTime\": \"2019-10-20T09:46:00+03:00\",\n" +
                "  \"duration\": \"PT1H\"\n" +
                "}"));

        mockMvc.perform(post("/api/staffs/1/reservations").contentType("application/json").content("{\n" +
                "  \"userName\": \"Sofia\",\n" +
                "  \"companyName\": \"ThoughtWorks\",\n" +
                "  \"zoneId\": \"Africa/Nairobi\",\n" +
                "  \"startTime\": \"2019-10-20T08:47:00+03:00\",\n" +
                "  \"duration\": \"PT2H\"\n" +
                "}"))
                .andExpect(jsonPath("$.message").value("The application is conflict with existing application."));

        mockMvc.perform(post("/api/staffs/1/reservations").contentType("application/json").content("{\n" +
                "  \"userName\": \"Sofia\",\n" +
                "  \"companyName\": \"ThoughtWorks\",\n" +
                "  \"zoneId\": \"Africa/Nairobi\",\n" +
                "  \"startTime\": \"2019-10-20T10:45:59+03:00\",\n" +
                "  \"duration\": \"PT1H\"\n" +
                "}")).andExpect(MockMvcResultMatchers.status().is(409));
    }
}
