package com.twuc.webApp;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.entity.Staff;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;


public class InsertAndGetRobInfoTest extends ApiTestBase{

    @Autowired
    private MockMvc mockMvc;

    @Test
    void should_return_201_if_insert_staff_record_success() throws Exception {
        mockMvc.perform(post("/api/staffs")
        .contentType("application/json")
        .content("{ \"firstName\": \"Rob\",\"lastName\": \"Hall\"}"))
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.header().string("Location","/api/staffs/1"));

    }

    @Test
    void should_return_400_if_insert_staff_record_with_null_lastName() throws Exception {
        mockMvc.perform(post("/api/staffs")
                .contentType("application/json")
                .content("{ \"firstName\": \"Rob\"}"))
                .andExpect(MockMvcResultMatchers.status().is(400));
    }

    @Test
    void should_return_400_if_insert_staff_record_firstName_larger_than_64() throws Exception {

        mockMvc.perform(post("/api/staffs")
                .contentType("application/json")
                .content("{ \"firstName\": \"Robabcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcd" +
                        "abcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcd" +
                        "abcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcd" +
                        "abcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcd" +
                        "abcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcd\",\"lastName\": \"Hall\"}"))
                .andExpect(MockMvcResultMatchers.status().is(400));
    }

    @Test
    void should_return_200_when_get_a_staff_record_if_succeeded() throws Exception {
        mockMvc.perform(post("/api/staffs")
                .contentType("application/json")
                .content("{ \"firstName\": \"Rob\",\"lastName\": \"Hall\"}"));

        mockMvc.perform(get("/api/staffs/1"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.firstName").value("Rob"))
                .andExpect(jsonPath("$.lastName").value("Hall"));
    }

    @Test
    void should_return_404_when_get_a_staff_record_if_staff_id_not_exists() throws Exception {
        mockMvc.perform(post("/api/staffs")
                .contentType("application/json")
                .content("{ \"firstName\": \"Rob\",\"lastName\": \"Hall\"}"));

        mockMvc.perform(get("/api/staffs/123"))
                .andExpect(MockMvcResultMatchers.status().is(404));
    }


    @Test
    void should_get_all_the_staff_records_when_GET_api() throws Exception {
        mockMvc.perform(post("/api/staffs")
                .contentType("application/json")
                .content("{ \"firstName\": \"Rob\",\"lastName\": \"Hall\"}"));

        mockMvc.perform(post("/api/staffs")
                .contentType("application/json")
                .content("{ \"firstName\": \"James\",\"lastName\": \"Bond\"}"));

        MockHttpServletResponse response = mockMvc.perform(get("/api/staffs")).andReturn().getResponse();
        List<Staff> staff = getStaff(response);
        assertEquals("1",staff.get(0).getId().toString());
        assertEquals("Hall", staff.get(0).getLastName());
        assertEquals("Rob", staff.get(0).getFirstName());
        assertEquals("2",staff.get(1).getId().toString());
        assertEquals("Bond", staff.get(1).getLastName());
        assertEquals("James", staff.get(1).getFirstName());

    }

    @Test
    void should_get_all_the_staff_records_with_id_in_order() throws Exception {
        mockMvc.perform(post("/api/staffs")
                .contentType("application/json")
                .content("{ \"firstName\": \"Rob\",\"lastName\": \"Hall\"}"));

        mockMvc.perform(post("/api/staffs")
                .contentType("application/json")
                .content("{ \"firstName\": \"James\",\"lastName\": \"Bond\"}"));

        MockHttpServletResponse response = mockMvc.perform(get("/api/staffs")).andReturn().getResponse();
        List<Staff> staff = getStaff(response);

        assertTrue(staff.get(0).getId() < staff.get(1).getId());
    }

    private List<Staff> getStaff(MockHttpServletResponse response) throws java.io.IOException {
        ObjectMapper objectMapper = new ObjectMapper();


        return objectMapper.reader()
                .forType(new TypeReference<List<Staff>>() {
                })
                .readValue(response.getContentAsString());
    }

    @Test
    void should_return_empty_list_when_get_all_the_staffs_if_no_staff_record_exists() throws Exception {
        MockHttpServletResponse response = mockMvc.perform(get("/api/staffs")).andReturn().getResponse();

        List<Staff> staff = getStaff(response);

        assertTrue(staff.isEmpty());

    }
}
