package com.twuc.webApp;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.zone.ZoneRulesProvider;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

public class GetTimeZoneListTest extends ApiTestBase {

    @Autowired
    private MockMvc mockMvc;


    @Test
    void should_get_time_zone() throws Exception {
        MockHttpServletResponse response = mockMvc.perform(get("/api/timezones"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andReturn().getResponse();
        ObjectMapper objectMapper = new ObjectMapper();
        Set<String> availableZoneIds = ZoneRulesProvider.getAvailableZoneIds();
        List<String> collectAvailableZoneIds = availableZoneIds.stream().sorted().collect(Collectors.toList());
        String expectedTimeZoneString = objectMapper.writeValueAsString(collectAvailableZoneIds);

        assertEquals(expectedTimeZoneString, response.getContentAsString());
    }
}
