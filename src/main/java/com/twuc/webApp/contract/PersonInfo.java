package com.twuc.webApp.contract;

public class PersonInfo {
    private String lastName;
    private String firstName;
    private String zoneId;

    public PersonInfo() {
    }

    public PersonInfo(String lastName, String firstName, String zoneId) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.zoneId = zoneId;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getZoneId() {
        return zoneId;
    }
}
