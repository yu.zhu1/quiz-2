package com.twuc.webApp.contract;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.DurationDeserializer;
import com.twuc.webApp.util.StartTime;

import java.time.Duration;

public class ReservationList {

    private String userName;

    private String companyName;

//    @JsonDeserialize(using = DurationDeserializer.class)
    private Duration duration;

    private StartTime startTime;

    public ReservationList() {
    }

    public ReservationList(String userName, String companyName, Duration duration, StartTime startTime) {
        this.userName = userName;
        this.companyName = companyName;
        this.duration = duration;
        this.startTime = startTime;
    }

    public String getUserName() {
        return userName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public Duration getDuration() {
        return duration;
    }

    public StartTime getStartTime() {
        return startTime;
    }
}
