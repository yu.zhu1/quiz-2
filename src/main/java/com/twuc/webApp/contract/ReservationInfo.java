package com.twuc.webApp.contract;


import java.time.Duration;
import java.time.ZonedDateTime;

public class ReservationInfo {

    private String userName;


    private String companyName;


    private String zoneId;


    private ZonedDateTime startTime;

    private Duration duration;

    public ReservationInfo() {
    }

    public ReservationInfo(String userName, String companyName, String zoneId, ZonedDateTime startTime, Duration duration) {
        this.userName = userName;
        this.companyName = companyName;
        this.zoneId = zoneId;
        this.startTime = startTime;
        this.duration = duration;
    }


    public String getUserName() {
        return userName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public String getZoneId() {
        return zoneId;
    }

    public ZonedDateTime getStartTime() {
        return startTime;
    }

    public Duration getDuration() {
        return duration;
    }


}
