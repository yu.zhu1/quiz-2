package com.twuc.webApp.repository;

import com.twuc.webApp.entity.Reservation;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ReservationRepository extends JpaRepository<Reservation, Long> {
    List<Reservation> findByStaffIdOrderByStartTime(Long staffId);
}
