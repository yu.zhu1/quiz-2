package com.twuc.webApp.web;

import com.twuc.webApp.contract.Message;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalErrorHandler {

    @ExceptionHandler({RuntimeException.class})
    ResponseEntity getError() {
        Message message = new Message();
        message.setMessage("The application duration should be within 1-3 hours.");
        return ResponseEntity.status(400).body(message);
    }
}
