package com.twuc.webApp.web.controller;

import com.twuc.webApp.util.ReservationZonedDateTime;
import com.twuc.webApp.util.StartTime;
import com.twuc.webApp.contract.*;
import com.twuc.webApp.entity.Reservation;
import com.twuc.webApp.entity.Staff;
import com.twuc.webApp.repository.ReservationRepository;
import com.twuc.webApp.repository.StaffRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.time.*;
import java.time.temporal.ChronoUnit;
import java.time.zone.ZoneRulesProvider;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
public class ReservationSystemController {

    private final StaffRepository staffRepository;

    public ReservationSystemController(StaffRepository staffRepository, ReservationRepository reservationRepository) {
        this.staffRepository = staffRepository;
        this.reservationRepository = reservationRepository;
    }

    private final ReservationRepository reservationRepository;


    @PostMapping("/api/staffs")
    ResponseEntity insertStaff(@RequestBody PersonInfo personInfo) {
        if (validStaffName(personInfo)) return ResponseEntity.status(400).build();

        String lastName = personInfo.getLastName();
        String firstName = personInfo.getFirstName();
        String zoneId = personInfo.getZoneId();
        Staff staff = new Staff(firstName, lastName, zoneId);

        staffRepository.save(staff);

        return ResponseEntity.status(201).header("Location", String.format("/api/staffs/%d", staff.getId())).body(staff);
    }

    private boolean validStaffName(@RequestBody PersonInfo personInfo) {
        if (StringUtils.isEmpty(personInfo.getFirstName()) || StringUtils.isEmpty(personInfo.getLastName())) {
            return true;
        }
        return personInfo.getFirstName().length() >= 64 || personInfo.getLastName().length() >= 64;
    }

    @GetMapping("/api/staffs/{id}")
    ResponseEntity getStaff(@PathVariable Long id) {
        Staff staff = staffRepository.findById(id).orElse(null);

        if (staff == null) {
            return ResponseEntity.status(404).build();
        }
        return ResponseEntity.status(200).body(staff);
    }

    @GetMapping("/api/staffs")
    ResponseEntity getStaffs() {
        List<Staff> staffList = staffRepository.findAll();
        return ResponseEntity.status(200).body(staffList);
    }

    @PutMapping("/api/staffs/{staffId}/timezone")
    ResponseEntity putTimeZone(@PathVariable Long staffId, @RequestBody PersonInfo personInfo) {
        if (personInfo.getZoneId() == null) {
            return ResponseEntity.status(400).build();
        }

        if (!ZoneRulesProvider.getAvailableZoneIds().contains(personInfo.getZoneId())) {
            return ResponseEntity.status(400).build();
        }


        Staff staff = staffRepository.findById(staffId).orElse(null);
        assert staff != null;
        staff.setZoneId(personInfo.getZoneId());
        staffRepository.save(staff);
        return ResponseEntity.status(200).body(staff);
    }

    @GetMapping("/api/timezones")
    ResponseEntity getTimeZones() {
        Set<String> availableZoneIds = ZoneRulesProvider.getAvailableZoneIds();
        List<String> collectAvailableZoneIds = availableZoneIds.stream().sorted().collect(Collectors.toList());
        return ResponseEntity.status(200).body(collectAvailableZoneIds);
    }

    @PostMapping("/api/staffs/{staffId}/reservations")
    ResponseEntity addReservation(@PathVariable Long staffId, @RequestBody ReservationInfo reservationInfo) {
        Staff staff = staffRepository.findById(staffId).orElse(null);

        Message message = new Message();

        assert staff != null;
        if (validZoneId(staff, message)) return ResponseEntity.status(409).body(message);

        String companyName = reservationInfo.getCompanyName();
        Duration duration = reservationInfo.getDuration();
        ZonedDateTime newReservationStartTime = reservationInfo.getStartTime();
        String userName = reservationInfo.getUserName();
        String zoneId = reservationInfo.getZoneId();


        if (reservationTooCloseFromNow(message, newReservationStartTime))
            return ResponseEntity.status(400).body(message);

        if (withinWorkingHours(staff, message, newReservationStartTime))
            return ResponseEntity.status(409).body(message);

        if (validDuration(message, duration)) return ResponseEntity.status(400).body(message);

        if (conflictReservation(staffId, message, duration, newReservationStartTime))
            return ResponseEntity.status(409).body(message);


        Reservation reservation = new Reservation(userName, companyName, zoneId, newReservationStartTime, duration);

        reservation.setStaff(staff);
        reservationRepository.save(reservation);

        return ResponseEntity.status(201).header("Location", String.format("/api/staffs/%d/reservations", staffId)).body(reservation);
    }

    private boolean validZoneId(Staff staff, Message message) {
        if (staff.getZoneId() == null) {
            message.setMessage(String.format("%s %s is not qualified.", staff.getFirstName(), staff.getLastName()));
            return true;
        }
        return false;
    }

    private boolean reservationTooCloseFromNow(Message message, ZonedDateTime newReservationStartTime) {
        long betweenHours = getBetweenHours(newReservationStartTime);
        if (betweenHours <= 48) {
            message.setMessage("Invalid time: the application is too close from now. The interval should be greater than 48 hours.");
            return true;
        }
        return false;
    }

    private boolean withinWorkingHours(Staff staff, Message message, ZonedDateTime newReservationStartTime) {
        ZonedDateTime staffStartTime = getStaffStartTime(newReservationStartTime, staff);
        if (staffStartTime.getHour() < 7 || staffStartTime.getHour() > 19) {
            message.setMessage("You know, our staff has their own life.");
            return true;
        }
        return false;
    }

    private boolean conflictReservation(@PathVariable Long staffId, Message message, Duration duration, ZonedDateTime newReservationStartTime) {
        long invalidTimePeriods = getInvalidTimePeriods(staffId, duration, newReservationStartTime);
        if (invalidTimePeriods != 0) {
            message.setMessage("The application is conflict with existing application.");
            return true;
        }
        return false;
    }

    private boolean validDuration(Message message, Duration duration) {
        if (duration.toHours() < 1 || duration.toHours() > 3) {
            message.setMessage("The application duration should be within 1-3 hours.");
            return true;
        }
        return false;
    }

    private long getInvalidTimePeriods(@PathVariable Long staffId, Duration duration, ZonedDateTime newReservationStartTime) {
        List<Reservation> reservations = reservationRepository.findByStaffIdOrderByStartTime(staffId);
        ZonedDateTime newReservationFinishTime = newReservationStartTime.plusHours(duration.toHours());
        return reservations.stream().filter(res -> res.getStartTime().getDayOfYear() == newReservationStartTime.getDayOfYear()).filter(res -> {
            ZonedDateTime reservedFinishTime = res.getStartTime().plusHours(res.getDuration().toHours());
            return !(newReservationFinishTime.isBefore(res.getStartTime()) || newReservationStartTime.isAfter(reservedFinishTime));
        }).count();
    }

    private long getBetweenHours(ZonedDateTime startTime) {
        LocalDateTime startLocalDateTime = startTime.toLocalDateTime();
        LocalDateTime nowLocalDateTime = LocalDateTime.now();
        return ChronoUnit.HOURS.between(nowLocalDateTime, startLocalDateTime);
    }

    @GetMapping("api/staffs/{staffId}/reservations")
    ResponseEntity getReservationList(@PathVariable Long staffId) {
        List<Reservation> reservations = reservationRepository.findByStaffIdOrderByStartTime(staffId);
        List<ReservationList> reservationList = reservations.stream().map(res -> getReservationList(staffId, res)).collect(Collectors.toList());
        return ResponseEntity.status(200).body(reservationList);
    }

    private ReservationList getReservationList(@PathVariable Long staffId, Reservation reservation) {
        String userName = reservation.getUserName();
        String companyName = reservation.getCompanyName();
        Duration duration = reservation.getDuration();
        ZonedDateTime clientStartTime = reservation.getStartTime();
        String clientZoneId = reservation.getZoneId();

        Staff staff = staffRepository.findById(staffId).orElse(null);
        assert staff != null;
        ZonedDateTime staffStartTime = getStaffStartTime(clientStartTime, staff);

        String staffZoneId = staff.getZoneId();
        ReservationZonedDateTime clientReservationZonedDateTime = new ReservationZonedDateTime(clientZoneId, clientStartTime);
        ReservationZonedDateTime staffReservationZonedDateTime = new ReservationZonedDateTime(staffZoneId, staffStartTime);


        StartTime startTime = new StartTime(clientReservationZonedDateTime, staffReservationZonedDateTime);

        return new ReservationList(userName, companyName, duration, startTime);
    }

    private ZonedDateTime getStaffStartTime(ZonedDateTime clientStartTime, Staff staff) {
        ZonedDateTime utcDate = clientStartTime.withZoneSameInstant(ZoneOffset.UTC);
        ZoneId staffZoneIdOfId = ZoneId.of(staff.getZoneId());
        return utcDate.withZoneSameInstant(staffZoneIdOfId);
    }


}
