package com.twuc.webApp.entity;

import javax.persistence.*;
import java.time.Duration;
import java.time.ZonedDateTime;

@Entity
public class Reservation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 128)
    private String userName;

    @Column(nullable = false, length = 64)
    private String companyName;

    @Column(nullable = false)
    private String zoneId;

    @Column(nullable = false)
    private ZonedDateTime startTime;

    @Column(nullable = false)
    private Duration duration;

    @ManyToOne
    private Staff staff;

    public Reservation() {
    }

    public Reservation(String userName, String companyName, String zoneId, ZonedDateTime startTime, Duration duration) {
        this.userName = userName;
        this.companyName = companyName;
        this.zoneId = zoneId;
        this.startTime = startTime;
        this.duration = duration;
    }

    public Long getId() {
        return id;
    }

    public String getUserName() {
        return userName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public String getZoneId() {
        return zoneId;
    }

    public ZonedDateTime getStartTime() {
        return startTime;
    }

    public Duration getDuration() {
        return duration;
    }

    public void setStaff(Staff staff) {
        this.staff = staff;
    }

    public Staff getStaff() {
        return staff;
    }
}
