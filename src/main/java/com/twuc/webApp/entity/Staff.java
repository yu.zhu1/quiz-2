package com.twuc.webApp.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Staff {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 64, nullable = false)
    private String lastName;
    @Column(length = 64, nullable = false)
    private String firstName;
    @Column(nullable = false)
    private String zoneId;

    public List<Reservation> getReservations() {
        return reservations;
    }

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy ="staff")
    private List<Reservation> reservations = new ArrayList<>();

    public void setZoneId(String zoneId) {
        this.zoneId = zoneId;
    }

    public Staff() {
    }


    public Staff(String firstName, String lastName, String zoneId) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.zoneId = zoneId;
    }

    public Long getId() {
        return id;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getZoneId() {
        return zoneId;
    }
}
