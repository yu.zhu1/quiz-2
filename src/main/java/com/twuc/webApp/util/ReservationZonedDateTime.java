package com.twuc.webApp.util;

import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class ReservationZonedDateTime {

    private String zoneId;

    private OffsetDateTime startTime;

    public ReservationZonedDateTime() {
    }

    public ReservationZonedDateTime(String zoneId, ZonedDateTime startTime) {
        this.zoneId = zoneId;
        this.startTime = startTime.withZoneSameLocal(ZoneId.of(zoneId)).toOffsetDateTime();
    }

    public String getZoneId() {
        return zoneId;
    }

    public OffsetDateTime getStartTime() {
        return startTime;
    }
}
