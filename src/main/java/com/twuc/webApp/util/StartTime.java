package com.twuc.webApp.util;

public class StartTime {

    private ReservationZonedDateTime client;

    private ReservationZonedDateTime staff;

    public StartTime() {
    }

    public StartTime(ReservationZonedDateTime client, ReservationZonedDateTime staff) {
        this.client = client;
        this.staff = staff;
    }

    public ReservationZonedDateTime getClient() {
        return client;
    }

    public ReservationZonedDateTime getStaff() {
        return staff;
    }
}

